_context.invoke(function (di) {
    di.getService('snippetManager').on('after-update', function (evt) {
        console.log(evt);
    });
});
var modalLinks = document.querySelectorAll("a[data-modal]");
function initModals(links) {
    links.forEach(function (el) { return modalLinksHandler(el); });
}
function modalLinksHandler(link) {
    var modalOverlay = document.querySelector(".modal-overlay." + link.getAttribute('data-modal'));
    var modal = modalOverlay.querySelector(".modal");
    link.onclick = function (ev) {
        if (modal.classList.contains('loaded')) {
            ev.preventDefault();
        }
        modalOverlay.classList.add('active');
        modal.classList.add('active');
    };
    modalOverlay.querySelector('.close-modal').onclick = function (ev) {
        modalOverlay.classList.remove("active");
        modal.classList.remove("active");
    };
}
initModals(modalLinks);
_context.invoke(function (di) {
    di.getService('snippetManager').on('after-update', function (evt) {
        var modalLinks = document.querySelectorAll("a[data-modal]");
        initModals(modalLinks);
    });
});
