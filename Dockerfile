FROM php:7-apache

RUN a2enmod rewrite

ENV DEPENDENCIES="libpq-dev"

RUN apt-get clean \
    && apt-get update \
    && apt-get install -y $DEPENDENCIES \
    && apt-get autoremove -y \
    && apt-get clean \
&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# db driver
RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo pdo_pgsql pgsql

EXPOSE 80 443 9001