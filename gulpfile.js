var gulp = require('gulp-help')(require('gulp'));
var less = require('gulp-less');
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var notify = require("gulp-notify");
var shell = require('gulp-shell');
var ts = require('gulp-typescript');

var assets = 'assets/';
var publicDir = 'public/';
var lessDir = assets + 'less/';
var tsDir = assets + 'typescript/';
var cssDir = publicDir + 'css/';
var jsDir = publicDir + 'js/';

// Preklad .less suborov projektu
gulp.task('less', 'Project less files compilation', function() {
    return gulp.src([lessDir + 'main.less'])
        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(cleanCSS({keepSpecialComments: 0}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(cssDir))
        .pipe(notify({message: "\nLess compiled.", onLast: true}));
});

gulp.task('migrate', 'Run migration', function () {
    return gulp.src('').pipe(new docker().runMigration());
});

gulp.task('ts', 'Compile Typescript to JS', function () {
    var tsProject = ts.createProject(tsDir + 'tsconfig.json');
    var tsResult = tsProject.src()
        .pipe(tsProject());

    return tsResult.js.pipe(gulp.dest(jsDir));
});

//FUNKCIE
var docker = function(ymlfile, container, project) {
    this.ymlfile = ymlfile || 'docker-compose.yml';
    this.container = container || 'web';
    this.project = project || false;

    var parent = this;
    var baseCommand = function() {
        var project = parent.project ? ' -p \'' + parent.project + '\' ' : '';
        return 'docker-compose ' + project + ' -f ' + parent.ymlfile + ' ';
    };

    // vrati pripraveny prikaz
    this.run = function(command, enviromental) {
        command = command || '';

        return shell(baseCommand() + 'run ' + this.container + ' ' + command);
    };

    this.runPHP = function (command) {
        return this.run('php ' + command);
    };

    this.runMigration = function() {
        return this.runPHP('www/index.php migration:migrate');
    };
};