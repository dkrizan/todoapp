declare let _context: any;

let modalLinks = document.querySelectorAll("a[data-modal]");

function initModals(links: NodeListOf<Element>) {
    links.forEach(el => modalLinksHandler(el as HTMLElement));
}

function modalLinksHandler(link: HTMLElement) {
    let modalOverlay = document.querySelector(".modal-overlay." + link.getAttribute('data-modal'));
    let modal = modalOverlay.querySelector(".modal");
    link.onclick = function (ev: MouseEvent) {
        if (modal.classList.contains('loaded')) {
            ev.preventDefault();
        }
        modalOverlay.classList.add('active');
        modal.classList.add('active');
    };
    (modalOverlay.querySelector('.close-modal') as HTMLElement).onclick = function(ev: MouseEvent) {
        modalOverlay.classList.remove("active");
        modal.classList.remove("active");
    };
}

initModals(modalLinks);


_context.invoke(function(di) {


    di.getService('snippetManager').on('after-update', function(evt) {
        let modalLinks = document.querySelectorAll("a[data-modal]");
        initModals(modalLinks);
    });
});

