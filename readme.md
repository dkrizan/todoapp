Todo app
=================

Simple todo app for school project.

Installation
------------

	composer install --ignore-platform-reqs
	npm install
	gulp less
	gulp migrate
	
Run docker with
    
    docker-compose up
    
Application is now available on [localhost:1234/www](localhost:1234/www).

Make directories `temp/` and `log/` writable.