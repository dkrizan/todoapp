<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180502192614 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE projects_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE tags_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE todos_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE users_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE projects (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE tags (id INT NOT NULL, name VARCHAR(255) NOT NULL, colour VARCHAR(255) NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE todos (id INT NOT NULL, tag INT DEFAULT NULL, project INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, deadline DATE NOT NULL, "user" INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CD826255356B3608 ON todos ("user")');
        $this->addSql('CREATE INDEX IDX_CD826255389B783 ON todos (tag)');
        $this->addSql('CREATE INDEX IDX_CD8262552FB3D0EE ON todos (project)');
        $this->addSql('CREATE TABLE users (id INT NOT NULL, login VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, full_name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE todos ADD CONSTRAINT FK_CD826255356B3608 FOREIGN KEY ("user") REFERENCES users ("id") NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE todos ADD CONSTRAINT FK_CD826255389B783 FOREIGN KEY (tag) REFERENCES tags (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE todos ADD CONSTRAINT FK_CD8262552FB3D0EE FOREIGN KEY (project) REFERENCES projects (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE todos DROP CONSTRAINT FK_CD8262552FB3D0EE');
        $this->addSql('ALTER TABLE todos DROP CONSTRAINT FK_CD826255389B783');
        $this->addSql('ALTER TABLE todos DROP CONSTRAINT FK_CD826255356B3608');
        $this->addSql('DROP SEQUENCE projects_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE tags_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE todos_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE users_id_seq CASCADE');
        $this->addSql('DROP TABLE projects');
        $this->addSql('DROP TABLE tags');
        $this->addSql('DROP TABLE todos');
        $this->addSql('DROP TABLE users');
    }
}
