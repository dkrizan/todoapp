<?php

namespace App\Entities;

use App\Libraries\IdentifiedEntity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @author dkrizan
 * @ORM\Entity
 * @ORM\Table(name="projects")
 *
 * @method string getName()
 * @method void setName(string $name)
 *
 * @method TodoItem[] getTodos()
 */
class Project extends IdentifiedEntity {

    /**
     * Názov
     * @ORM\Column(name="name", type="string", nullable=false)
     * @var string
     */
    protected $name;

    /**
     * Todos
     * @ORM\OneToMany(targetEntity="\App\Entities\TodoItem", mappedBy="project")
     * @var ArrayCollection | TodoItem[]
     */
    protected $todos;

    public function __construct() {
        parent::__construct();
        $this->todos = new ArrayCollection();
    }
}