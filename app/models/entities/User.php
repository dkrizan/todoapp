<?php

namespace App\Entities;

use App\Libraries\IdentifiedEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author dkrizan
 * @ORM\Entity
 * @ORM\Table(name="users")
 *
 * @method string getFullName()
 * @method void setFullName(string $name)
 *
 * @method string getLogin()
 * @method void setLogin(string $login)
 *
 * @method string getPassword()
 * @method void setPassword(string $password)
 */
class User extends IdentifiedEntity {

    /**
     * Login
     * @ORM\Column(name="login", type="string", nullable=false)
     * @var string
     */
    protected $login;

    /**
     * Heslo
     * @ORM\Column(name="password", type="string", nullable=false)
     * @var string
     */
    protected $password;

    /**
     * Celé meno
     * @ORM\Column(name="full_name", type="string", nullable=false)
     * @var string
     */
    protected $fullName;

}