<?php

namespace App\Entities;

use App\Libraries\IdentifiedEntity;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;

/**
 * @author dkrizan
 * @ORM\Entity
 * @ORM\Table(name="todos")
 *
 * @method string getName()
 * @method void setName(string $name)
 *
 * @method string getUser()
 * @method void setUser(User $user)
 *
 * @method Tag getTag()
 * @method void setTag(Tag $tag)
 *
 * @method string getDescription()
 * @method void setDescription(string $description)
 *
 * @method Project getProject()
 * @method void setProject(Project $project)
 *
 * @method void setDeadline(DateTime $deadline)
 */
class TodoItem extends IdentifiedEntity {

    /**
     * Názov
     * @ORM\Column(name="name", type="string", nullable=false)
     * @var string
     */
    protected $name;

    /**
     * Popis
     * @ORM\Column(name="description", type="text", nullable=true)
     * @var string
     */
    protected $description;

    /**
     * Dátum
     * @ORM\Column(name="deadline", type="date", nullable=false)
     * @var DateTime
     */
    protected $deadline;

    /**
     * Užívateľ
     * @ORM\ManyToOne(targetEntity="\App\Entities\User")
     * @ORM\JoinColumn("`user`", referencedColumnName="id")
     * @var User
     */
    protected $user;

    /**
     * Tag
     * @ORM\ManyToOne(targetEntity="\App\Entities\Tag")
     * @ORM\JoinColumn("tag", referencedColumnName="id")
     * @var Tag
     */
    protected $tag;

    /**
     * Užívateľ
     * @ORM\ManyToOne(targetEntity="\App\Entities\Project")
     * @ORM\JoinColumn("project", referencedColumnName="id")
     * @var Project
     */
    protected $project;

    public function getDeadline() {
        return $this->deadline->format('d.m.Y');
    }
}