<?php

namespace App\Entities;

use App\Libraries\IdentifiedEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author dkrizan
 * @ORM\Entity
 * @ORM\Table(name="tags")
 *
 * @method string getName()
 * @method void setName(string $name)
 *
 */
class Tag extends IdentifiedEntity {

    /**
     * Názov
     * @ORM\Column(name="name", type="string", nullable=false)
     * @var string
     */
    protected $name;

    /**
     * Farba
     * @ORM\Column(name="colour", type="string", nullable=true)
     * @var string
     */
    protected $colour;
}