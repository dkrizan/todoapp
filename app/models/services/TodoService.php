<?php

namespace Services;

use App\Entities\TodoItem;
use App\Libraries\BaseService;
use Nette\Utils\DateTime;

/**
 * Class TodoService
 * @package Services
 * @author Daniel Krizan <danyelkrizan@gmail.com>
 */
class TodoService extends BaseService {
    /**
     * @var ProjectService
     */
    private $projectService;
    /**
     * @var TagService
     */
    private $tagService;

    /**
     * TodoService constructor.
     * @param \Kdyby\Doctrine\EntityManager $em
     * @param ProjectService $projectService
     * @param TagService $tagService
     */
    public function __construct(\Kdyby\Doctrine\EntityManager $em, ProjectService $projectService, TagService $tagService)
    {
        parent::__construct($em, TodoItem::class);
        $this->projectService = $projectService;
        $this->tagService = $tagService;
    }

    /**
     * @param $entity TodoItem
     * @param array $values
     * @return TodoItem
     */
    public function update($entity, $values) {
        $entity->setName($values['name']);
        if (isset($values['project'])) {
            $entity->setProject($this->projectService->find($values['project']));
        }
        if (isset($values['tag'])) {
            $entity->setTag($this->tagService->find($values['tag']));
        }
        $entity->setDescription($values['description']);
        $entity->setDeadline(DateTime::createFromFormat('d.m.Y', $values['deadline']));
        return $entity;
    }
}