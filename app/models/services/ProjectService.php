<?php

namespace Services;

use App\Entities\Project;
use App\Libraries\BaseService;

/**
 * Class ProjectService
 * @package Services
 * @author Daniel Krizan <danyelkrizan@gmail.com>
 */
class ProjectService extends BaseService {

    public function __construct(\Kdyby\Doctrine\EntityManager $em)
    {
        parent::__construct($em, Project::class);
    }

    /**
     * @param Project $entity
     * @param array $values
     * @return Project
     */
    public function update($entity, $values)
    {
        $entity->setName($values['name']);
        return $entity;
    }
}