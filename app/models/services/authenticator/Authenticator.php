<?php


namespace Services\Authenticator;


use App\Entities\User;
use Kdyby\Doctrine\EntityRepository;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Services\UsersService;

/**
 * Class Authenticator
 * @package Services\Authenticator
 * @author Daniel Krizan <danyelkrizan@gmail.com>
 */
class Authenticator implements IAuthenticator
{

    const SALT = "vo_fs65d4f";

    /** @var UsersService $usersService */
    private $usersService;

    /** @var string */
    private $salt;

    /**
     * Authenticator constructor.
     * @param EntityRepository $usersService
     * @param string $salt
     */
    public function __construct(EntityRepository $usersService, $salt = self::SALT) {
        $this->usersService = $usersService;
        $this->salt = $salt;
    }

    /**
     * Performs an authentication
     * @param  array
     * @return \Nette\Security\Identity
     * @throws AuthenticationException
     */
    public function authenticate(array $credentials) {
        list($login, $password) = $credentials;
        /** @var User $user */
        $user = $this->usersService->findOneBy(['login' => $login]);

        if (isset($user) && $user->getPassword() !== self::calculateHash($password)) {
            throw new AuthenticationException("Bad credentials", self::INVALID_CREDENTIAL);
        } elseif ($user == NULL) {
            throw new \UserNotFoundException();
        }

        return new Identity(
            $user->getId(),
            NULL,
            [
                'login' => $user->getLogin(),
                'fullName' => $user->getFullName()
            ]
        );
    }

    /**
     * @param $login
     * @param $password
     * @throws AuthenticationException
     * @return User
     */
    public function verifyCredentials($login, $password) {
        /** @var User $user */
        $user = $this->usersService->findOneBy(['login' => $login]);
        if ((!$user || $user->getPassword() !== self::calculateHash($password))) {
            throw new AuthenticationException("Bad credentials", self::INVALID_CREDENTIAL);
        }
        return $user;
    }

    /**
     * Computes salted password hash.
     * @param  string
     * @return string
     */
    public static function calculateHash($password) {
        return hash('sha512', $password . str_repeat(self::SALT, 2));
    }
}