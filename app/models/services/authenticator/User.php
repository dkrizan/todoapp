<?php


namespace Services\Authenticator;

use Nette\Security\IAuthenticator;
use Nette\Security\IAuthorizator;
use Nette\Security\IUserStorage;

/**
 * Class User
 * @package Services\Authenticator
 * @author Daniel Krizan <danyelkrizan@gmail.com>
 */
class User extends \Nette\Security\User {

    /**
     * User constructor.
     * @param IUserStorage $userStorage
     * @param IAuthenticator|NULL $authenticator
     * @param IAuthorizator|NULL $authorizator
     */
    public function __construct(IUserStorage $userStorage, IAuthenticator $authenticator = NULL, IAuthorizator $authorizator = NULL) {
        parent::__construct($userStorage, $authenticator, $authorizator);
    }

    /**
     * @param null $id
     * @param null $password
     */
    public function login($id = NULL, $password = NULL) {
        parent::login($id, $password);
        $loginHash = $this->generateLoginHash();
        $this->getIdentity()->loginHash = $loginHash;
    }

    /**
     * @param bool $clearIdentity
     */
    public function logout($clearIdentity = false)
    {
        parent::logout($clearIdentity);
    }

    /**
     * Generuje hash
     * @return string
     */
    public function generateLoginHash() {
        return md5(uniqid() . microtime() . rand());
    }
}