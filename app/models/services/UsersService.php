<?php

namespace Services;

use App\Entities\Project;
use App\Entities\Tag;
use App\Entities\User;
use App\Libraries\BaseService;
use Services\Authenticator\Authenticator;

/**
 * Class UsersService
 * @package Services
 * @author Daniel Krizan <danyelkrizan@gmail.com>
 */
class UsersService extends BaseService {

    protected $user;

    /**
     * UsersService constructor.
     * @param \Kdyby\Doctrine\EntityManager $em
     * @param Authenticator\User $user
     */
    public function __construct(\Kdyby\Doctrine\EntityManager $em, \Services\Authenticator\User $user)
    {
        parent::__construct($em, User::class);
        $this->user = $user;
    }

    /**
     * @param User $entity
     * @param array $values
     * @return User
     */
    public function update($entity, $values)
    {
        $entity->setLogin($values['login']);
        $entity->setFullName($values['fullName']);
        $entity->setPassword(Authenticator::calculateHash($values['password']));
        return $entity;
    }

    /**
     * Return user entity
     * @return User|Object
     */
    public function getLoggedUser() {
        return $this->find($this->user->id);
    }

}