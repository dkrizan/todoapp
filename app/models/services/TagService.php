<?php

namespace Services;

use App\Entities\Project;
use App\Entities\Tag;
use App\Libraries\BaseService;

/**
 * Class TagService
 * @package Services
 * @author Daniel Krizan <danyelkrizan@gmail.com>
 */
class TagService extends BaseService {

    public function __construct(\Kdyby\Doctrine\EntityManager $em)
    {
        parent::__construct($em, Tag::class);
    }

    /**
     * @param Tag $entity
     * @param array $values
     * @return Tag
     */
    public function update($entity, $values)
    {
        $entity->setName($values['name']);
        return $entity;
    }

    /**
     * Tags to selectbox items
     * @return array
     */
    public function toSelectBoxAll() {
        $source = [];
        foreach ($this->findAll() as $tag) {
            $source[$tag->getId()] = $tag->getName();
        }
        return $source;
    }
}