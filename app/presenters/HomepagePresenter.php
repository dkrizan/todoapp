<?php

namespace App\Presenters;

use App\Components\Todos\ITodoModalFactory;
use App\Components\Todos\TodoFormModal;
use App\Components\Todos\TodoModal;
use App\Entities\Project;
use App\Entities\TodoItem;
use App\Libraries\BasePresenter;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Nette\Application\AbortException;
use Nette\Application\UI\Form;
use Services\ProjectService;
use Services\TagService;
use Services\TodoService;
use Tracy\Debugger;


class HomepagePresenter extends BasePresenter
{

    /** @var TodoService */
    protected $todoService;

    /** @var ProjectService */
    protected $projectService;

    /** @var TagService */
    protected $tagService;

    /** @var ITodoModalFactory @inject */
    public $todoModalFactory;

    public function beforeRender()
    {
        parent::beforeRender();
        $this->template->projects = $this->projectService->findAll();
        $this->template->tags = $this->tagService->findAll();
    }

    public function actionList($projectId) {
        $this['todoModal']['form']['project']->setValue($projectId);
        $this->redrawControl('menu');
        $this->redrawControl('content');
        $this->redrawControl('header');
    }

    public function renderList($projectId) {
        /** @var Project $project */
        $project = $this->projectService->find($projectId);
        $this->template->todos = $project->getTodos();
        $this->template->projectId = $projectId;
    }

    public function createComponentAddProjectForm() {
        $form = new Form();
        $form->setTranslator($this->translator);

        $form->addText('name')->setAttribute('placeholder', 'app.menu.projectName');
        $form->addSubmit('submit', '+')->setAttribute('class', 'pure-primary-button pure-button');

        $form->onSuccess[] = [$this, 'addProjectSuccess'];
        return $form;
    }

    public function addProjectSuccess(Form $form, $values) {
        $project = $this->projectService->create($values);
        $this->projectService->persist($project);
        $this->projectService->flush();
        $this->redrawControl('projects');
    }

    public function createComponentAddTagForm() {
        $form = new Form();

        $form->setTranslator($this->translator);
        $form->addText('name')->setAttribute('placeholder', '');
        $form->addSubmit('submit', '+')->setAttribute('class', 'primary-button pure-button');

        $form->onSuccess[] = [$this, 'addTagSuccess'];
        return $form;
    }

    public function addTagSuccess(Form $form, $values) {
        /** @var TodoItem $todo */
        $todo = $this->tagService->create($values);
        $this->tagService->persist($todo);
        $this->tagService->flush();
        $this->redrawControl('tags');
    }



    /**
     * Remove project
     * @param $projectId
     */
    public function handleRemoveProject($projectId) {
        try {
            $project = $this->projectService->find($projectId);
            $this->projectService->delete($project);
            $this->projectService->flush();
			$this->redirect('Homepage:default');
        } catch (ForeignKeyConstraintViolationException $ex) {
            $this->flashMessage('app.flash.projectHasTodos', 'error');
        } catch (\Exception $ex) {
            $this->log($ex);
            if ($ex instanceof AbortException) {
                throw new $ex;
            }
        }

	}

    /**
     * Remove todoItem
     * @param $todoId
     */
    public function handleRemoveTodo($todoId) {
        try {
            $todo = $this->todoService->find($todoId);
            $this->todoService->delete($todo);
            $this->todoService->flush();
        } catch (\Exception $ex) {
            $this->log($ex);
        }
        $this->redrawControl(null, false);
        $this->redrawControl('list');
    }

    /**
     * Remove tag
     * @param $tagId
     */
    public function handleRemoveTag($tagId) {
        $this->redrawControl(null, false);
        $this->redrawControl('tags');
        try {
            $tag = $this->tagService->find($tagId);
            $this->tagService->delete($tag);
            $this->tagService->flush();
        } catch (ForeignKeyConstraintViolationException $ex) {
            $this->flashMessage('app.flash.tagIsUsed', 'error');
        } catch (\Exception $ex) {
            $this->log($ex);
        }
    }

    /**
     * Creates modal for with "todo" form
     * @return TodoModal
     */
    public function createComponentTodoModal() {
        $modal = $this->todoModalFactory->create();
        $modal->getForm()->onSuccess[] = array($this, 'todoSuccess');
        return $modal;
    }

    /**
     * "Todo" form callback on success
     * @param Form $form
     * @param $values
     */
    public function todoSuccess(Form $form, $values) {
        /** @var TodoItem $todo */
        $todo = $this->todoService->create($values);
        $this->todoService->persist($todo);
        $this->todoService->flush();
        $this->redrawControl(null, false);
        $this->redrawControl('list');
    }

    public function injectTodoService(TodoService $todoService) {
        $this->todoService = $todoService;
    }

    public function injectProjectService(ProjectService $projectService) {
        $this->projectService = $projectService;
    }

    public function injectTagService(TagService $tagService) {
        $this->tagService = $tagService;
    }
}
