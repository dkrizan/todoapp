<?php

namespace App\Presenters;

use App\Libraries\BasePresenter;
use Nette\Application\AbortException;
use Nette\Application\UI\Form;
use Services\Authenticator\Authenticator;
use Services\UsersService;


class SignPresenter extends BasePresenter
{

    /** @var UsersService */
    protected $usersService;

    /** @var Authenticator */
    protected $authenticator;

	public function actionUp() {
        $this->redrawControl('body');
    }
	
	public function actionIn() {
        $this->redrawControl('body');
    }

	
    public function createComponentSignInForm() {
        $form = new Form();

        $form->setTranslator($this->translator);
        $form->addText('login', 'app.form.login');
        $form->addPassword('password', 'app.form.password');

        $form->addSubmit('submit', 'app.button.login')->setAttribute('class', 'pure-button pure-button-primary');

        $form->onSuccess[] = [$this, 'signInSuccess'];
        return $form;
    }

    public function signInSuccess(Form $form, $values) {
	    $this->redrawControl(null, false);
        try {
            $this->getUser()->login($values['login'], $values['password']);
            $this->redirect(':Homepage:default');
        } catch (\UserNotFoundException $ex) {
            $this->flashMessage('app.flash.userNotFound');
        } catch (\Exception $ex) {
            $this->log($ex);
            if ($ex instanceof AbortException) {
                throw $ex;
            }
        }
    }

    public function createComponentSignUpForm() {
        $form = new Form();

        $form->setTranslator($this->translator);
        $form->addText('login', 'app.form.login');
        $form->addText('fullName', 'app.form.fullName');
        $form->addPassword('password', 'app.form.password')
            ->setRequired('app.flash.fillInPassword');
        $form->addPassword('password2', 'app.form.passwordAgain')
            ->setRequired('app.flash.fillInPasswordSecondTime')
            ->addRule(Form::EQUAL, 'app.flash.passwordsNotSame', $form['password']);

        $form->addSubmit('submit', 'app.button.signUp')->setAttribute('class', 'pure-button pure-button-primary');

        $form->onSuccess[] = [$this, 'signUpSuccess'];
        return $form;
    }

    public function signUpSuccess(Form $form, $values) {
        try {
            $user = $this->usersService->create($values);
            $this->usersService->persist($user);
            $this->usersService->flush();
            $this->redrawControl('body');
            $this->redirect('Sign:in');
        } catch (\Exception $ex) {
            if ($ex instanceof AbortException) {
                throw $ex;
            }
            $this->log($ex);
        }
    }


    public function injectUsersService(UsersService $usersService) {
        $this->usersService = $usersService;
    }

    public function injectAuthenticator(Authenticator $authenticator) {
        $this->authenticator = $authenticator;
    }

}
