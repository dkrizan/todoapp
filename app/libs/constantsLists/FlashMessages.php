<?php


namespace App\Libraries;

/**
 * Class FlashMessagesTrait
 * @package App\Libraries\ConstantsLists
 * @author Daniel Krizan <danyelkrizan@gmail.com>
 */
class FlashMessages extends BaseConstantList
{
    const FLASH_SUCCESS = "success";
    const FLASH_ERROR = "error";
    const FLASH_INFO = "warning";
    const FLASH_WARNING = "success";

    static public $labels = array(
        self::FLASH_SUCCESS => "flash.type.success",
        self::FLASH_ERROR => "flash.type.error",
        self::FLASH_INFO => "flash.type.info",
        self::FLASH_WARNING => "flash.type.warning",
     );

}