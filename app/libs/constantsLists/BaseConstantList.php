<?php


namespace App\Libraries;

/**
 * Class BaseConstantList
 * @package App\Libraries
 * @author Daniel Krizan <danyelkrizan@gmail.com>
 */
class BaseConstantList
{
    static public $labels;

    static public function getLabels() {
        return static::$labels;
    }

    static public function getLabel($key) {
        return static::$labels[$key];
    }
}