<?php

namespace App\Libraries;

use Kdyby\Doctrine\EntityManager;
use Tracy\Debugger;

/**
 * Class BaseService
 * @package Libraries
 * @author Daniel Krizan <danyelkrizan@gmail.com>
 */
abstract class BaseService {

    /** @var EntityManager */
    private $em;

    /** @var string */
    private $repositoryName;

    /**
     * BaseService constructor.
     * @param EntityManager $em
     * @param $repositoryName
     */
    public function __construct(EntityManager $em, $repositoryName) {
        $this->repositoryName = $repositoryName;
        $this->em = $em;
    }

    /**
     * Get entity repository
     * @return \Kdyby\Doctrine\EntityRepository
     */
    final protected function getRepository() {
        return $this->em->getRepository($this->repositoryName);
    }

    /**
     * Find all entities
     * @return null|IdentifiedEntity[]
     */
    public function findAll() {
        return $this->getRepository()->findAll();
    }

    /**
     * Find one instance of entity
     * @param $id
     * @return null|object|IdentifiedEntity
     */
    public function find($id) {
        return $this->getRepository()->find($id);
    }

    /**
     * Find By
     * @param $criteria
     * @param $orderBy
     * @return array
     */
    public function findBy($criteria, $orderBy = null) {
        return $this->getRepository()->findBy($criteria, $orderBy);
    }

    /**
     * Find One By
     * @param $criteria
     * @param $orderBy
     * @return array
     */
    public function findOneBy($criteria, $orderBy = null) {
        return $this->getRepository()->findOneBy($criteria, $orderBy);
    }

    /**
     * Create entity
     * @param $values array|object
     * @return IdentifiedEntity|object
     */
    public function create($values) {
        $entity = $this->getRepository()->getClassMetadata()->newInstance();
        $this->update($entity, $values);
        return $entity;
    }

    /**
     * Remove entity
     * @param $entity
     */
    public function delete($entity) {
        $this->em->remove($entity);
    }

    /**
     * Persist entity
     * @param IdentifiedEntity $entity
     * @return $this
     */
    public function persist($entity) {
        $this->em->persist($entity);
        return $this;
    }

    /**
     * Flush to db
     * @return $this
     * @throws \Exception
     */
    public function flush() {
        $this->em->flush();
        return $this;
    }

    /**
     * @param $entity IdentifiedEntity|object
     * @param array $values|object
     * @return mixed
     */
    abstract public function update($entity, $values);

}