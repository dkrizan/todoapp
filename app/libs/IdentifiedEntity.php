<?php

namespace App\Libraries;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * @author: dkrizan
 * @ORM\MappedSuperclass()
 *
 * @property-read int $id
 */
abstract class IdentifiedEntity extends BaseEntity {

    /**
     * ID entity
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @var integer
     */
    protected $id;

    protected $salt;

    public function __construct() {
        $this->salt = uniqid();
    }

    /**
     * @return integer
     */
    final public function getId() {
        return $this->id;
    }

    /**
     * Klonovanie entity
     */
    public function __clone() {
        $this->id = NULL;
    }
}
