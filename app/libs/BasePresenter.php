<?php
/**
 * Zakladny presenter
 * @author: Daniel Krizan <danyelkrizan@gmail.com>
 */

namespace App\Libraries;


use Nette\Application\UI\Presenter;
use Tracy\Debugger;

class BasePresenter extends \Nittro\Bridges\NittroUI\Presenter {

    const DEFAULT_SNIPPETS = ["body"];

    /** @persistent */
    public $locale;

    /** @var \Kdyby\Translation\Translator @inject */
    public $translator;

    public function __construct() {
        parent::__construct();
    }

    public function startup()
    {
        parent::startup();
        if (!$this->user->isLoggedIn() && $this->presenter->getName() != "Sign" && $this->presenter->getAction() != "in") {
            $this->redirect('Sign:in');
        }
        $this->setDefaultSnippets(self::DEFAULT_SNIPPETS);
        $params = $this->getContext()->getParameters();
        $this->template->publicDir = $params['publicDir'];
    }

    public function log(\Exception $ex) {
        Debugger::log($ex);
    }

    /**
     * User logout
     */
    public function handleLogout() {
        $this->getUser()->logout();
        $this->redirect('Sign:in');
    }

    /**
     * Refresh page
     */
    public function handleRefresh() {
        $this->redirect('this');
    }

    /**
     * @param $message
     * @param string $type
     * @return \stdClass
     */
    public function flashMessage($message, $type = 'info')
    {
        $messages[] = $flash = (object) [
            'message' => $this->translator->translate($message),
            'type' => $type,
            'title' => $this->translator->translate(FlashMessages::getLabel($type))
        ];
        $this->getTemplate()->flashes = $messages;
        $this->redrawControl('flash');
        return $flash;
    }
}