<?php

namespace App\Components\Todos;

use App\Components\FormModal;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Form;
use Services\TagService;

class TodoModal extends FormModal {

    /**
     * @var TagService
     */
    private $tagService;

    /**
     * TodoFormModal constructor.
     * @param Translator $translator
     * @param TagService $tagService
     */
    public function __construct(Translator $translator, TagService $tagService)
    {
        $this->tagService = $tagService;
        parent::__construct($translator);
		$this->setLinkTitle("app.menu.addTodo");
    }

    public function render() {
        $this->template->setFile(__DIR__ . '/todoModal.latte');
        parent::render();
    }

    public function initForm() {
        $form = $this->getForm();

        $form->addHidden('project');
        $form->addText('name', 'app.form.title');
        $form->addTextArea('description', 'app.form.description');
        $form->addText('deadline', 'app.form.deadline')->setAttribute('placeholder', 'dd.mm.yyyy')->addRule(Form::PATTERN, 'app.form.badFormat', '^[0-3]?[0-9].[0-3]?[0-9].(?:[0-9]{2})?[0-9]{2}$')->setRequired();
        $form->addSelect('tag', 'app.form.tag', $this->tagService->toSelectBoxAll());

        $form->addSubmit('submit', 'app.menu.addTodo')->setAttribute('class', 'primary-button pure-button');
    }
}

interface ITodoModalFactory {
    /**
     * @return TodoModal
     */
    public function create();
}