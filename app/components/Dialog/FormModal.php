<?php

namespace App\Components;

use Kdyby\Translation\Translator;
use Nette\Application\UI\Form;

abstract class FormModal extends Modal {
    /**
     * FormModal constructor.
     * @param Translator $translator
     */
    public function __construct(Translator $translator) {
        parent::__construct($translator);
        $this->initForm();
    }

    /**
     * Creates form control
     * @return Form
     */
    public function createComponentForm() {
        $form = new Form();
        $form->setTranslator($this->translator);
        $form->onSuccess[] = array($this, 'close');
        return $form;
    }

    /**
     * Returns form
     * @return Form
     */
    public function getForm() {
        return $this['form'];
    }

    /**
     * Init form controls
     * @return mixed
     */
    abstract public function initForm();
}