<?php

namespace App\Components;

use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;
use Nette\Utils\Html;
use Tracy\Debugger;

abstract class Modal extends Control
{

    /**
     * Set modal opened/closed
     * @var bool
     */
    protected $active = false;

    /**
     * Modal element ID used to connect modal with link to open it
     * @var string
     */
    protected $id;

    /**
     * Modal css classes
     * @var string
     */
    protected $class = "";

    /**
     * Path to layout template
     * @var string
     */
    protected $layout;

    /**
     * Title of link to open modal
     * @var string
     */
    protected $linkTitle = "Open modal";

    /**
     * @var Translator
     */
    protected $translator;

    /**
     * Modal constructor.
     * @param Translator $translator
     */
    public function __construct(Translator $translator) {
        parent::__construct();
        $this->translator = $translator;
        $this->setLayout(__DIR__ . '/templates/@layout.latte');
    }

    public function attached($presenter) {
        parent::attached($presenter);
        $this->setId($this->getUniqueId());
    }

    /**
     * Render method
     */
    public function render() {
        $this->template->active = $this->active;
        $this->template->class = $this->class;
        $this->template->id = $this->id;
        $this->template->render();
    }

    /**
     * Handler to open modal
     */
    public function handleOpen() {
        $this->presenter->redrawControl(null, false);
        $this->redrawControl('modal');
        $this->open();
    }

    /**
     * Open modal
     */
    public function open() {
        $this->active = true;
    }

    /**
     * Close modal
     */
    public function close() {
        $this->active = false;
        $this->redrawControl();
    }

    /**
     * Get path to layout
     * @return string
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * Sets path to layout
     * @param string $layout
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    /**
     * Creates link for opening modal
     * @return Html|string
     */
    public function generateLink() {
        $link = Html::el('a');
        $link->setAttribute('href', $this->link('open'));
        $link->setAttribute('data-modal', $this->getUniqueId());
        $link->setText($this->linkTitle);
        return $link;
    }

    public function renderLink() {
        $this->template->setFile(__DIR__ . '/templates/link.latte');
        $this->template->link = $this->generateLink();
        $this->template->render();
    }

    /**
     * Set link title
     * @param string $linkTitle
     */
    public function setLinkTitle($linkTitle)
    {
        $this->linkTitle = $this->translator->translate($linkTitle);
    }

    /**
     * Set modal class
     * @param string $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * Sets modal element ID
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}